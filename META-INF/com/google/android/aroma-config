##
# Fix Colorspace Issue
# You can use one of following values:
#   "rgba", "abgr", "argb", "bgra"

ini_set("force_colorspace","rgba");

##
# Initializing Information
#

ini_set("rom_name",             "BatMan-Rom™ ");
ini_set("rom_version",          "V1.4.0");
ini_set("rom_author",           "Tkkg1994");
ini_set("rom_device",           "S7/S8");
ini_set("rom_date",             "14.10.2017");

##
# Show Animated Splash
#
anisplash(
#  #-- Number of Loop
    1,
  
  #-- Frame  [ Image, duration in millisecond ]
    "anim/a00", 0,
    "anim/a01", 0,
    "anim/a02", 0,
    "anim/a03", 0,
    "anim/a04", 0,
	"anim/a05", 0,
	"anim/a06", 0,
	"anim/a07", 0,
	"anim/a08", 0,
	"anim/a09", 0,
	"anim/a10", 0,
	"anim/a11", 0,
	"anim/a12", 0,
	"anim/a13", 0,
	"anim/a14", 0,
	"anim/a15", 1000
);

##
# Font and Theme Selection
#

fontresload( "0", "ttf/Roboto-Regular.ttf;ttf/DroidSansArabic.ttf;ttf/DroidSansFallback.ttf;", "12" ); #-- Use sets of font (Font Family)
theme("Batman");

##
# SET LANGUAGE & FONT FAMILY
#

loadlang("langs/en.lang");
fontresload( "0", "ttf/Roboto-Regular.ttf", "12" ); #-- "0" = Small Font ( Look at Fonts & UNICODE Demo Below )
fontresload( "1", "ttf/Roboto-Regular.ttf", "18" ); #-- "1" = Big Font

##
#   SHOW ROM/Mod INFORMATION
#
viewbox(
	#-- Title
	"<~welcome.title>",

 	#-- Text
	"<~welcome.text1> <b>"+
	#-- Get Config Value
	ini_get("rom_name")+
	"</b> <~common.for> <b>"+ini_get("rom_device")+"</b>.\n\n"+

	"<~welcome.text2>\n\n"+
	
	"  <~welcome.version>\t: <b><#selectbg_g>"+ini_get("rom_version")+"</#></b>\n"+
	"  <~welcome.updated>\t: <b><#selectbg_g>"+ini_get("rom_date")+"</#></b>\n\n\n"+

	"<~welcome.next>",

	#-- Icon
	"@welcome"
);

##
# LICENSE
#

agreebox(
	#-- Title
	"BatMan-Rom™",

	#-- Subtitle / Description
	"Read Carefully",

	#-- Icon:
	"@license",

	#-- Text Content 
	resread("license.txt"),

	#-- Checkbox Text
	"Have you really read it and are agree?",

	#-- Unchecked Alert Message
	"You've to agree!"
);

##
# MAIN MENU- INSTALLER n MISC
#
menubox(
	#-- Title
	"BatMan-Rom™ Menu",

	#-- Sub Title
	"Please select from the Menu Below to Modify the required features",

	#-- Icon
	"@apps",
 
	#-- Will be saved in /tmp/aroma/menu.prop
	"menu.prop",
    
     #-------------------------+-----------------[ Menubox Items ]-------------------------+---------------#
     # TITLE                   |  SUBTITLE                                                 |   Item Icons  #
     #-------------------------+-----------------------------------------------------------+---------------#	
	
	"Installation",		"ROM Installation with Various Features - RECOMMENDED",		"@install",      #-- selected = 1
	"System Info",		"Get and show device/partition informations",			"@info",         #-- selected = 2
	"ChangeLog",		"BatMan ChangeLog",						"@agreement",    #-- selected = 3
	"Quit Install",		"Leaving the Aroma setup to install another Rom :(",		"@install"       #-- selected = 4

);

##
# System Info 
#

if prop("menu.prop","selected")=="2" then

	#-- Show Please Wait
	pleasewait("Getting System Information...");

	#-- Fetch System Information
	setvar(
		#-- Variable Name
		"sysinfo",

		#-- Variable Value
		"<@center><b>Your Device System Information</b></@>\n\n"+

		"Device Name\t\t: <#ffe400>Galaxy S8/S8+</#>\n"+
		"Board Name\t\t: <#ffe400>SM-G950F/G955F</#>\n"+
		"Manufacturer\t\t: <#ffe400>Samsung</#>\n"+
	  
		"\n"+

		"System Size\t\t: <b><#selectbg_g>"+getdisksize("/system","m")+" MB</#></b>\n"+
		"\tFree\t\t: <b><#selectbg_g>"+getdiskfree("/system","m")+" MB</#></b>\n\n"+
		"Data Size\t\t: <b><#selectbg_g>"+getdisksize("/data","m")+" MB</#></b>\n"+
		"\tFree\t\t: <b><#selectbg_g>"+getdiskfree("/data","m")+" MB</#></b>\n\n"+
		"SDCard Size\t\t: <b><#selectbg_g>"+getdisksize("/sdcard","m")+" MB</#></b>\n"+
		"\tFree\t\t: <b><#selectbg_g>"+getdiskfree("/sdcard","m")+" MB</#></b>\n\n"+

		""
	);
  
	#-- Show Textbox
	textbox(
		#-- Title
		"System Information",
    
		#-- Subtitle
		"Current system Information on your S8",
    
		#-- Icon
		"@info",
    
		#-- Text
		getvar("sysinfo")
	);
  
	#-- Show Alert
	alert(
		#-- Alert Title
		"Finished",
    
		#-- Alert Text
		"You will be back to Menu",
    
		#-- Alert Icon
		"@alert"
	);
  
	#-- Back to Menu ( 2 Wizard UI to Back )
	back("2");
  
endif;

##
# CHANGELOG DISPLAY
#

if prop("menu.prop","selected")=="3" then

	#-- TextDialog 
	textdialog(
		#-- Title
		"BatMan-Rom Changelog",
		#-- Text
		resread("changelog.txt"),
		#-- Custom OK Button Text (Optional)
		"Close"
	);
 
	#-- Back to Menu ( 2 Wizard UI to Back )
	back("1");
  
endif;

##
# QUIT INSTALLER
#

if prop("menu.prop","selected")=="4" then

#-- Exit
	if confirm(
		#-- Title
		"Exit",
		#-- Text
		"Are you sure want to exit the Installer?",
		#-- Icon (Optional)
		"@alert"
	)=="yes"
	then
		#-- Exit 
		exit("");
	endif;
endif;


##
#  Select Type of Install
#

if prop("menu.prop","selected")=="1" then

# Write tmp file for restore check
writetmpfile("restore.prop","restore=no");
writetmpfile("jumpinstall.prop","install=no");

# Check if wipe.sh is backed up from last time
resexec("checkconfig/checkconfig.sh");

if prop("restore.prop","restore")=="yes" then

	if confirm(
			#-- Title
			"Backup found!",
			#-- Text
			"Do you want to restore your settings from last aroma installation?",
			#-- Icon (Optional)
			"@install",
			#-- Yes Text
			"Yes, restore backup.",
			#-- No Text
			"No, start fresh."
	)=="yes"
		then
			resexec("checkconfig/restoreconfig.sh");
			writetmpfile("jumpinstall.prop","install=yes");
			alert("Result","Backup restored");
		else
			writetmpfile("jumpinstall.prop","install=no");
			alert("Result","Backup not restored, going on with fresh installation");
	endif;
	if prop("jumpinstall.prop","install")=="yes" then
		if confirm(
				#-- Title
				"Jump to installation screen?",
				#-- Text
				"Do you want to jump directly to install screen? Since you already got an aroma backup.",
				#-- Icon (Optional)
				"@install",
				#-- Yes Text
				"Yes, keep all settings.",
				#-- No Text
				"No, change settings."
		)=="yes"
			then
				writetmpfile("jumpinstall.prop","install=yes");
			else
				writetmpfile("jumpinstall.prop","install=no");
		endif;
	endif;
endif;

if prop("jumpinstall.prop","install")=="no" then

##
# Wipe Form
#

form(
    "Wipe",
    "Choose your wipe option here",
    "@install",
    "wipe.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"wipe",		"Wipe method",		"",									"group",
	"full",		"Full Wipe",		"Wipes everything except internal and external SD card",		"select.selected",
	"clean",	"Clean Install",	"Wipes everything except external SD card",				"select",
	"dirty",	"Dirty Flash",		"Wipes only system (there will be bugs with this). Can also be used to change settings after clean install!",			"select"
);

##
# Kernel Form
#

form(
    "Kernel/Recovery",
    "Choose your desired kernel here. You can also update recovery if you want",
    "@install",
    "kernel.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"kernel",	"Kernel Selection",		"",									"group",
	"stock",	"Stock Kernel",			"Full stock kernel",							"select",
	"batstock",	"BatStock-Kernel",		"Latest BatStock Kernel",						"select.selected",
	"recovery",	"Recovery Update",		"",									"group",
	"update",	"Update Recovery",		"TWRP to latest official 3.1.1-2, (this MUST be installed on S8 devices!)",	"select.selected",
	"keep",		"Keep current Recovery",	"Will keep your current installed TWRP version",			"select"
);

##
# buildtweaks.prop Form
#

form(
    "Build.prop and init.d Tweaks",
    "You can choose if you want to install any build.prop or init.d tweaks",
    "@install",
    "buildtweaks.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"build",	"Select build.prop tweaks",	"",											"group",
	"finger",	"Fingerprint after reboot",	"Enables fingerprint right after a reboot without entering your password first (only on modded SystemUI)",	"check.checked",
	"user",		"Multiuser",			"Enable Multi User in settings and in statusbar",					"check.checked",
	"fix",		"Screen mirror fix",		"Applies a screen mirror fix (was present all the time before)",			"check.checked",
	"navbar",	"S8 Navigation Bar",		"Will show and enable the S8 navbar (won't change anything on the S8, this is only for the s7)",	"check",
	"s8prop",	"S8 build.prop",		"Device will be recognized as s8, introduces several bugs (won't change anything on the S8, this is only for the s7)",	"check"
);

alert("Important:", "Snapchat can work with root, if you select Magisk V14.2 below! Follow the steps provided on XDA to get Safetynet working.");

##
# Magisk Form
#

form(
    "Root/Magisk",
    "You can choose many things here, read carefully:",
    "@install",
    "magisk.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"magisk",	"Magisk V14.2",		"",									"group",
	"yes",		"Yes",			"Magisk V14.2 will be installed with MagiskSU",				"select.selected",
	"no",		"No",			"Won't install Magisk",							"select",
	"root",		"Root method", 		"",									"group",
	"su",		"SuperSU 2.82-SR5",	"Will install SuperSU 2.82 SR5",					"select",
	"magisk",	"MagiskSU",		"Installs MagiskSU based on phh root",					"select.selected",
	"phh",		"Phh's r310",		"Installs normal r310 phh superuser",					"select",
	"no",		"No additional root",	"I don't want any root method to be installed",				"select",
	"busybox",	"Busybox", 		"",									"group",
	"osm",		"Busybox by osmosis",	"Installs busybox, needed for RomControl",				"select.selected",
	"yds",		"Busybox by YashdSaraf","Installs busybox, needed for RomControl",				"select",
	"no",		"No busybox",		"Don't install busybox",						"select",
	"xposed",	"Xposed", 		"",									"group",
	"xpsma",	"Magisk module 88.1",	"Installs Magisk version of Xposed, safetynet will FAIL!!",		"select",
	"no",		"No Xposed",		"Recommend right now, many things neeed to be tested on xposed (can give bugs)",	"select.selected"
);

if (prop("magisk.prop","xposed")=="ofs" || prop("magisk.prop","xposed")=="xpsma") then
alert("Attention:", "You selected Xposed during the install, please be aware that this will break safetynet, probably kill romcontrol mods and slows down the system/more battery drain. Also I won't support Xposed related questions in the threads on XDA/GrifoDev!");
endif;

##
# Sound Form
#

form(
    "Dual Speaker/Camera Mod",
    "Choose to install dual speaker mod and camera mod",
    "@install",
    "sound.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"sound",	"Sound Level",			"",								"group",
	"mod",		"Dual Speaker mod",		"Installs latest dual speaker mod",				"select",
	"stock",	"Use stock sound",		"Will keep the stock sound files",				"select.selected",
	"camera",	"Camera Mod",			"",								"group",
	"mod",		"Modded camera",		"Installs modded camera by zeroprobe with advanced quality",	"select",
	"stock",	"Stock camera",			"Will keep the stock camera files",				"select.selected"
);

##
# Emoji Form
#

form(
    "Emojis",
    "Do you want to install the iOS, nougat or stock Emojis in keyboard?",
    "@install",
    "emoji.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"emoji",	"Select which Icons you want",	"",							"group",
	"ios",		"Install iOS 10.2 emojis",	"Will install new iOS emojis in samsung keyboard",	"select",
	"stock",	"Install stock emoji",		"Will display stock emojis in samsung keyboard",	"select.selected"
);

##
# SystemUI/Settings Form
#

form(
    "SystemUI and Settings",
    "You can choose here if you want to install modded systemUI and settings",
    "@install",
    "mods.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"systemUI",	"Choose your prefered SystemUI",		"",								"group",
	"modded",	"Modded SystemUI",		"Includes all romcontrol features, disabled sd card notificaitons, fingerprint after reboot mode and many more features.",		"select.selected",
	"stock",	"Stock SystemUI",		"No features included in here. Just stock SystemUI as you can find on any rom",	"select",
	"settings",	"Choose your prefered Settings",		"",								"group",
	"modded",	"Modded Settings",		"BatMan logo, LED indicator, outodoor mode mode enabled",			"select.selected",
	"stock",	"Stock Settings",		"No special features, all stock in this settings",				"select"
);

##
# Modem Form
#

form(
    "Modem and Bootloader",
    "You want to auto update your modem and bootloader?",
    "@install",
    "cpandbl.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"modem",	"Modem update F/FD",		"",												"group",
	"update",	"Update Modem",			"Will auto update your modem to latest AQI7/DQIC on F/FD",					"select.selected",
	"keep",		"Keep current Modem",		"Will keep your current modem and change nothing",						"select",
	"bootloader",	"Bootloader update F/FD",	"",												"group",
	"update",	"Update Bootloader",		"Will auto update your BL to AQI7/DQIC on F/FD models.",					"select.selected",
	"keep",		"Keep current Bootloader",	"Will keep your current bootloader and change nothing",						"select"
);

##
# Bootanimation Form
#

form(
    "Splash and Bootanim",
    "Choose Splash screen and boot animation here",
    "@install",
    "bootanim.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"anim",		"Bootanimations",			"",											"group",
	"stock",	"Stock animation",		"Just as always, stock boot/shutdown animation",						"select",
	"bat_black",	"BatMan black animation",	"Brand new BatMan animation thanks to the greatest myellow!",					"select.selected",
	"splash",	"Splash Screen",		"",												"group",
	"stock",	"Stock Splash",			"S8 splash screen will be installed on your s7/s8",						"select",
	"bat_black",	"BatMan black Splash",		"New BatMan black Splash will be installed",							"select",
	"nothing",	"Keep current",			"Keeps your current splash screen (no matter which it is)",					"select.selected"
);

##
# AdAway Form
#

form(
    "AdAway",
    "Select if you want adblocker by default or not",
    "@install",
    "adaway.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"adblocker",	"Adblocker",		"",											"group",
	"enable",	"Enable by default",	"Enables Adblocker by default (3minit battery needs disabled adblocker to work)",	"select.selected",
	"disable",	"Disable by default",	"Disables Adblocker by default. However you can still enable it in the app later",	"select"
);

alert("Attention", "Removing samsung apps can lead to force closes and not working features!");

##
# Sub Checkbox 1
#

checkbox(
  #-- Title
	"Bloat Samsung Options",
	  
  #-- Sub Title
    "Everything what is ticked will be installed!",
	  
  #-- Icon:
     "@install",
	 
  #-- Will be saved in /tmp/aroma/samsung.prop
	"samsung.prop",

	  #----------------------------------[ Selectbox With Groups ]-----------------------------------#
	  # TITLE            |  SUBTITLE                                                 | Initial Value #
	  #------------------+-----------------------------------------------------------+---------------#  

	"Samsung Apps",		"",									2,	#-- group 1
	"Allshare",		"Allshare Cast by Samsung",						1,	#-- item.1.1
	"ANT",			"ANT-Hal and Plugins",							1,	#-- item.1.2
	"Notes",		"Notes App from Samsung",						1,	#-- item.1.3
	"Mirrorlink",		"",									1,	#-- item.1.4
	"SBrowser",		"Samsungs own Browser",							1,	#-- item.1.5
	"Samsung Calendar",	"Planner App and Widget",						1,	#-- item.1.6
	"Video Editor",		"",									1,	#-- item.1.7
	"Weather",		"Weather Widget",							1,	#-- item.1.8
	"Calculator",		"",									1,	#-- item.1.9
	"Email",		"Samsungs default Mail program",					1,	#-- item.1.10
	"Galaxy Store",		"Samsungs own Galaxy App",						1,	#-- item.1.11
	"Gallery",		"Samsungs Gallery App",							1,	#-- item.1.12
	"Game",			"Samsungs Game Centre",							1,	#-- item.1.13
	"MyFiles",		"Myfiles Application",							1,	#-- item.1.14
	"SHealth",		"Tracks fitness and more",						1,	#-- item.1.15
	"Themestore",		"",									1,	#-- item.1.16
	"VoiceNote",		"",									1,	#-- item.1.17
	"SmartSwitch",		"",									0,	#-- item.1.18
	"Gear S manager", 	"Used for Gear S, Gear S2 etc",						1,	#-- item.1.19
	"Gear VR manager", 	"You have to install this in order to use your oculus gear VR",		1,	#-- item.1.20
	"Microsoft Excel",	"",									0,	#-- item.1.21
	"Microsoft PowerPoint",	"",									0,	#-- item.1.22
	"Microsoft Word",	"",									0,	#-- item.1.23
	"Knox",			"",									0,	#-- item.1.24
	"Samsung OneDrive",	"",									0,	#-- item.1.25
	"Samsung Connect",	"",									0,	#-- item.1.26
	"Samsung Members",	"",									0,	#-- item.1.27
	"Bixby",		"Samsungs new Al Assistant",						1,	#-- item.1.28
	"Dex",			"Installs Samsungs dex launcher",					0,	#-- item.1.29
	"Rom Control",		"To control various mods of the rom (mainly on modded UI)",		1,	#-- item.1.30
	"Samsung Music",	"Most recent samsung music apk with color mods",			1,	#-- item.1.31
	"Samsung Fonts",	"Adds 100+ new fonts into your font selection in settings",		0,	#-- item.1.32
	"Activity Zone",	"Enables activity zone from latest s6 galaxy active",			0	#-- item.1.33

	  #--------[ Initial Value = 0: Unselected, 1: Selected, 2: Group Item, 3: Not Visible ]---------#
);

##
# Sub Checkbox 2
#

checkbox(
  #-- Title
	"Bloat Google Options",
	  
  #-- Sub Title
    "Everything what is ticked will be installed!",
	  
  #-- Icon:
     "@install",
	 
  #-- Will be saved in /tmp/aroma/google.prop
	"google.prop",

	  #----------------------------------[ Selectbox With Groups ]-----------------------------------#
	  # TITLE            |  SUBTITLE                                                 | Initial Value #
	  #------------------+-----------------------------------------------------------+---------------#  

	"Google Apps",		"",									2,	#-- group 1
	"Google Now",		"",									0,	#-- item.1.1
	"Google Drive",		"",									0,	#-- item.1.2
	"Gmail",		"",									0,	#-- item.1.3
	"Google Duo",		"",									0,	#-- item.1.4
	"Google Maps",		"",									0,	#-- item.1.5
	"Play Music",		"",									0,	#-- item.1.6
	"Google Movies",	"",									0,	#-- item.1.7
	"Youtube",		"",									0,	#-- item.1.8
	"Google Photos",	"",									0	#-- item.1.9

	  #--------[ Initial Value = 0: Unselected, 1: Selected, 2: Group Item, 3: Not Visible ]---------#
);

##
# CSC Form
#

form(
    "Multi-CSC",
    "You have selected a multi-csc!",
    "@install",
    "csc.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"CSC",		"OMC Multi-CSC",	"",							"group",
	"ACR",		"ACR",			"Ghana unbranded",					"select",
	"AFG",		"AFG",			"Afghanistan unbranded",				"select",
	"AFR",		"AFR",			"Kenya unbranded",					"select",
	"ARO",		"ARO",			"Argentinia unbranded",					"select",
	"ATO",		"ATO",			"Open Austria unbranded",				"select",
	"AUT",		"AUT",			"Switzerland unbranded",				"select",
	"BGL",		"BGL",			"Bulgaria unbranded",					"select",
	"BNG",		"BNG",			"Bangladesh unbranded",					"select",
	"BRI",		"BRI",			"Taiwan unbranded",					"select",
	"BTC",		"BTC",			"Libya unbranded",					"select",
	"BTU",		"BTU",			"United Kingdom unbranded",				"select",
	"CAC",		"CAC",			"Uzbekistan unbranded",					"select",
	"CAM",		"CAM",			"Cambodia unbranded",					"select",
	"CEL",		"CEL",			"Isreal Cellcom branded",				"select",
	"CHO",		"CHO",			"Chile unbranded",					"select",
	"COO",		"COO",			"Colombia unbranded",					"select",
	"CPW",		"CPW",			"United Kingdom (unknown)",				"select",
	"DBT",		"DBT",			"Germany unbranded (default)",				"select.selected",
	"DKR",		"DKR",			"Senegal unbranded",					"select",
	"ECT",		"ECT",			"Nigeria unbranded",					"select",
	"EGY",		"EGY",			"Egypt unbranded",					"select",
	"EON",		"EON",			"Trinidad and Tobago unbranded",			"select",
	"ETL",		"ETL",			"Czech Republic unbranded",				"select",
	"EUR",		"EUR",			"Greece unbranded",					"select",
	"GLB",		"GLB",			"Philippines Globe branded",				"select",
	"ILO",		"ILO",			"Israel unbranded",					"select",
	"INS",		"INS",			"Indian unbranded",					"select",
	"ITV",		"ITV",			"Italy unbranded",					"select",
	"KSA",		"KSA",			"Saudi Arabia unbranded",				"select",
	"LAO",		"LAO",			"Thailand (unknown)",					"select",
	"LUX",		"LUX",			"Luxembourg unbranded",					"select",
	"LYS",		"LYS",			"United Arab Emirates unbranded",			"select",
	"MAT",		"MAT",			"Morrocco MAT branded",					"select",
	"MID",		"MID",			"Iraq unbranded",					"select",
	"MM1",		"MM1",			"Singapore MM1 branded",				"select",
	"MWD",		"MWD",			"Morrocco unbranded",					"select",
	"MYM",		"MYM",			"Thailand (unknown)",					"select",
	"NEE",		"NEE",			"Nordic Countries unbranded",				"select",
	"NPL",		"NPL",			"Nepal unbranded",					"select",
	"ORX",		"ORX",			"Slovakia unbranded",					"select",
	"PAK",		"PAK",			"Pakistan unbranded",					"select",
	"PCL",		"PCL",			"Israel Pelephone branded",				"select",
	"PHE",		"PHE",			"Spain unbranded",					"select",
	"PHN",		"PHN",			"Netherlands unbranded",				"select",
	"PTR",		"PTR",			"Israel Orange and Partner branded",			"select",
	"ROM",		"ROM",			"Romania unbranded",					"select",
	"SEB",		"SEB",			"Latvia unbranded",					"select",
	"SEE",		"SEE",			"South East Europe unbranded",				"select",
	"SEK",		"SEK",			"Ukraine unbranded",					"select",
	"SER",		"SER",			"Russia unbranded",					"select",
	"SIN",		"SIN",			"Singapore SingTel branded",				"select",
	"SMA",		"SMA",			"Philippines Smart branded",				"select",
	"SMP",		"SMP",			"WorldWide Telecom branded",				"select",
	"STH",		"STH",			"Singapore StarHub branded",				"select",
	"SKZ",		"SKZ",			"Kazakhstan unbranded",					"select",
	"SLK",		"SLK",			"Srilanka unbranded",					"select",
	"TEB",		"TEB",			"Bosnia and Herzegovina unbranded",			"select",
	"THL",		"THL",			"Thailand unbranded",					"select",
	"THR",		"THR",			"Iran unbranded",					"select",
	"TMC",		"TMC",			"Algeria unbranded",					"select",
	"TPA",		"TPA",			"Panama unbranded",					"select",
	"TPH",		"TPH",			"Portugal unbranded",					"select",
	"TTT",		"TTT",			"Trinidad and Tobago unbranded",			"select",
	"TUN",		"TUN",			"Tunisia unbranded",					"select",
	"TUR",		"TUR",			"Turkey unbranded",					"select",
	"WTL",		"WTL",			"Saudi Arabia unbranded",				"select",
	"XEF",		"XEF",			"France unbranded",					"select",
	"XEH",		"XEH",			"Hungary unbranded",					"select",
	"XEO",		"XEO",			"Poland unbranded",					"select",
	"XEU",		"XEU",			"United Kingdom EE & Three branded",			"select",
	"XFA",		"XFA",			"South Africa unbranded",				"select",
	"XFE",		"XFE",			"South Africa unbranded",				"select",
	"XME",		"XME",			"Malaysia unbranded",					"select",
	"XSG",		"XSG",			"United Arab Emirates unbranded",			"select",
	"XSP",		"XSP",			"Singapore unbranded",					"select",
	"XTC",		"XTC",			"Philippines Open Line branded",			"select",
	"XXV",		"XXV",			"Vietnam unbranded",					"select",
	"ZTO",		"ZTO",			"Brazil unbranded",					"select"
);

##
# csctweaks.prop Form
#

form(
    "CSC Features",
    "Choose which CSC features you want to enable",
    "@install",
    "csctweaks.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"csc",		"Select csc mods",		"",											"group",
	"cameracall",	"Camera during call",		"Enables camera while calling, can be used even during the call",			"check",
	"camerashutter","Camera Shutter",		"Enables sound menu in camera settings to adjust shutter",				"check",
	"airmessage",	"Air Message",			"Adds AirMessage to default settings",							"check",
	"savesms",	"SMS backup",			"Enables backup for sms inside the default messaging app",				"check",
	"disablemms",	"SMS to MMS convertion",	"Disables auto conversion from sms to mms",						"check",
	"smstweaks",	"More SMS options",		"Enables delivery/sent time of sms, resize quality of imgs",				"check",
	"blockmenu",	"Call blocking",		"Enables an additional setting to adjust blocking numbers",				"check",
	"networkspeed",	"Network Speed",		"Adds a settings entry to enable network traffic speed",				"check",
	"lteonly",	"LTE Only",			"Enables LTE only mode in connection settings",						"check",
	"smartmanager",	"SmartManager tweaks",		"Enables various things, for example Applock, trafficmanager, data saving and more",	"check",
	"powerplanning","PowerPlanning",		"This adds a power planning feature from the Galaxy J7 Max",				"check",
	"signalbar",	"Show 5 Signal bars",		"This has been reported to may break wifi calling!",					"check",
	"recent",	"Recent App Protection",	"",											"check",
	"datausage",	"Data Usage",			"Shows data usage in pulldown menu",							"check",
	"recording",	"Call Recorder",		"Enables call recorder inside the phone app",						"check",
	"wifi",		"Advanced wifi",		"Enables an additional wifi settings menu",						"check"
);

# This endif comes from the jump command
endif;

# Installation UI

ini_set("text_next", "Install Now");
ini_set("icon_next", "@installbutton");
  
viewbox(
	#-- Title
	"Ready to Install",

	#-- Text
	"BatMan-Rom is ready to be installed.\n\n"+
	"Press <b>Install ROM</b> to begin the installation.\n\n"+
	"To review or change any of your installation settings, press <b>Back</b>.\n\n"+
	"Press Menu -> Quit Installation to quit.",

	#-- Icon
	"@install"
);

endif;

##
# INSTALLATION PROCESS
#

if prop("menu.prop","selected")== "1" then

ini_set("text_next", "Next");
ini_set("icon_next", "@next");

install(
	"BatMan-Rom™ Installation",
	getvar("rom_name") + "\n" +
	"Please wait while BatMan-Rom is conquering your phone" +
	"",
	"@install"
);

ini_set("text_next", "Finish");
ini_set("icon_next", "@finish");
endif;
