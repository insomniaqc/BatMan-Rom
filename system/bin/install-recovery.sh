#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/11120000.ufs/by-name/RECOVERY:37830656:e8868d6c10585b7e16c5bc366d896b13ae67dade; then
  applypatch EMMC:/dev/block/platform/11120000.ufs/by-name/BOOT:33263616:b9fe412439117bbb44bd97ce1043eaa6b5fed9b4 EMMC:/dev/block/platform/11120000.ufs/by-name/RECOVERY e8868d6c10585b7e16c5bc366d896b13ae67dade 37830656 b9fe412439117bbb44bd97ce1043eaa6b5fed9b4:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
