#!/sbin/sh
# Written by Tkkg1994

getprop ro.boot.bootloader >> /tmp/BLmodel

if grep -q G93 /tmp/BLmodel; then
	mount /dev/block/platform/155a0000.ufs/by-name/SYSTEM /system
	mount /dev/block/platform/155a0000.ufs/by-name/USERDATA /data
	mount /dev/block/platform/155a0000.ufs/by-name/EFS /efs
else
	mount /dev/block/platform/11120000.ufs/by-name/SYSTEM /system
	mount /dev/block/platform/11120000.ufs/by-name/USERDATA /data
	mount /dev/block/platform/11120000.ufs/by-name/EFS /efs
fi

if [ ! -d /data/media/0/BatMan ]; then
	mkdir /data/media/0/BatMan
	mkdir /data/media/0/BatMan/EFS
fi

if [ -e /data/media/0/BatMan/EFS/efs.img ]; then
	dd if=/dev/block/sda3 of=/data/media/0/BatMan/EFS/efs_new.img bs=4096
	cp /system/build.prop /data/media/0/BatMan/EFS/build.prop_new
	echo "EFS backup already exist, create efs_new.img"
else
	dd if=/dev/block/sda3 of=/data/media/0/BatMan/EFS/efs.img bs=4096
	cp /system/build.prop /data/media/0/BatMan/EFS/build.prop
	echo "Created EFS backup"
fi

exit 10
